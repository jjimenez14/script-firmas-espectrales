import {  IsBoolean, IsNumber, IsOptional, IsPositive } from "class-validator";

export class PaginationDtoQuery{
    @IsOptional()
    @IsNumber()
    @IsPositive()
    Page:number=1;

    @IsOptional()
    @IsNumber()
    @IsPositive()
    Elementos:number=10;

    @IsOptional()
    @IsBoolean()
    Unactives:boolean=false;

/*    @IsOptional()
    @IsBoolean()
    OnlyUnactives:boolean=false;*/
}