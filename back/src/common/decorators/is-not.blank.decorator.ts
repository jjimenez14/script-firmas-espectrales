import { registerDecorator, ValidationOptions, ValidationArguments } from 'class-validator';

export function NotBlank( validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'NotBlank',
      target: object.constructor,
      propertyName: propertyName,

      options: validationOptions,
      validator: {
        validate(value: any) {
         
          if(typeof value !== 'string') return false;
          const valueTrim = value.replace(/ /g, " ");
          if(valueTrim ==='') return false;
          return true;
                 
        },
      },
    });
  };
}