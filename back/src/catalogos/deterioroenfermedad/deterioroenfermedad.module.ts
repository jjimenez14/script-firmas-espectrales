import { Module } from '@nestjs/common';
import { DanioenfermedadplagaService } from './danioenfermedadplaga.service';
import { DeterioroEnfermedadController } from './deterioroenfermedad.controller';

@Module({
  providers: [DanioenfermedadplagaService],
  controllers: [DeterioroEnfermedadController]
})
export class DeterioroEnfermedadModule {}
