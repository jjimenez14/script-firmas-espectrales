import { Module } from '@nestjs/common';
import { EspecieplagaService } from './especieplaga.service';
import { EspecieplagaController } from './especieplaga.controller';

@Module({
  providers: [EspecieplagaService],
  controllers: [EspecieplagaController]
})
export class EspecieplagaModule {}
