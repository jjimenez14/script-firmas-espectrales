import { Module } from '@nestjs/common';
import { SubtipodeficienciaService } from './subtipodeficiencia.service';
import { SubtipodeficienciaController } from './subtipodeficiencia.controller';

@Module({
  providers: [SubtipodeficienciaService],
  controllers: [SubtipodeficienciaController]
})
export class SubtipodeficienciaModule {}
