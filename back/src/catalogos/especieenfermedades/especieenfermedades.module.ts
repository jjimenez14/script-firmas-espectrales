import { Module } from '@nestjs/common';
import { EspecieenfermedadesService } from './especieenfermedades.service';
import { EspecieenfermedadesController } from './especieenfermedades.controller';

@Module({
  providers: [EspecieenfermedadesService],
  controllers: [EspecieenfermedadesController]
})
export class EspecieenfermedadesModule {}
