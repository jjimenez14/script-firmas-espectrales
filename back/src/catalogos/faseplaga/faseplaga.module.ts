import { Module } from '@nestjs/common';
import { FaseplagaService } from './faseplaga.service';
import { FaseplagaController } from './faseplaga.controller';

@Module({
  providers: [FaseplagaService],
  controllers: [FaseplagaController]
})
export class FaseplagaModule {}
