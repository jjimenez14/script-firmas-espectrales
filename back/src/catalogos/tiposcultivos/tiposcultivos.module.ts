import { Module } from '@nestjs/common';
import { TiposcultivosService } from './tiposcultivos.service';
import { TiposcultivosController } from './tiposcultivos.controller';

@Module({
  providers: [TiposcultivosService],
  imports: [TiposcultivosModule],
  controllers: [TiposcultivosController]
})
export class TiposcultivosModule {}
