import { Module } from '@nestjs/common';
import { TiposcultivosModule } from './tiposcultivos/tiposcultivos.module';
import { SubtiposcultivosModule } from './subtiposcultivos/subtiposcultivos.module';
import { EtapasfenologicasModule } from './etapasfenologicas/etapasfenologicas.module';
import { SubtiposenfermedadesModule } from './subtiposenfermedades/subtiposenfermedades.module';
import { EspecieenfermedadesModule } from './especieenfermedades/especieenfermedades.module';
import { FaseenfermedadplagaModule } from './faseenfermedad/faseenfermedad.module';
import { DeterioroEnfermedadModule } from './deterioroenfermedad/deterioroenfermedad.module';
import { SubtipoplagaModule } from './subtipoplaga/subtipoplaga.module';
import { EspecieplagaModule } from './especieplaga/especieplaga.module';
import { SubtipodeficienciaModule } from './subtipodeficiencia/subtipodeficiencia.module';
import { FaseplagaModule } from './faseplaga/faseplaga.module';
import { DeterioroplagaModule } from './deterioroplaga/deterioroplaga.module';
import { DeteriorodeficienciaModule } from './deteriorodeficiencia/deteriorodeficiencia.module';
import { FasedeficienciaModule } from './fasedeficiencia/fasedeficiencia.module';

@Module({
  imports: [TiposcultivosModule, SubtiposcultivosModule, EtapasfenologicasModule, SubtiposenfermedadesModule, EspecieenfermedadesModule, FaseenfermedadplagaModule, DeterioroEnfermedadModule, SubtipoplagaModule, EspecieplagaModule, SubtipodeficienciaModule, FaseplagaModule, DeterioroplagaModule, DeteriorodeficienciaModule, FasedeficienciaModule]
})
export class CatalogosModule {}
