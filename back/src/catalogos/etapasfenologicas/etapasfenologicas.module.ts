import { Module } from '@nestjs/common';
import { EtapasfenologicasService } from './etapasfenologicas.service';
import { EtapasfenologicasController } from './etapasfenologicas.controller';

@Module({
  providers: [EtapasfenologicasService],
  controllers: [EtapasfenologicasController]
})
export class EtapasfenologicasModule {}
