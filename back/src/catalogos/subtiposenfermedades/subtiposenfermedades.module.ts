import { Module } from '@nestjs/common';
import { SubtiposenfermedadesService } from './subtiposenfermedades.service';
import { SubtiposenfermedadesController } from './subtiposenfermedades.controller';

@Module({
  providers: [SubtiposenfermedadesService],
  controllers: [SubtiposenfermedadesController]
})
export class SubtiposenfermedadesModule {}
