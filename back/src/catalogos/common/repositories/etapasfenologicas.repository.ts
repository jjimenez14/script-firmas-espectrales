import { EntityRepository, Repository } from "typeorm";
import { EtapasFenologicas } from "../entities/etapasfenologicas.entity";

@EntityRepository(EtapasFenologicas)
export class EtapasFenologicasRepository extends Repository<EtapasFenologicas>{

}