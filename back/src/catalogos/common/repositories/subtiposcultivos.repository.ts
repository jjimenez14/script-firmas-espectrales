import { EntityRepository, Repository } from "typeorm";
import { SubTipoCultivo } from "../entities/subtiposcultivos.entity";

@EntityRepository(SubTipoCultivo)
export class SubTiposRepositoryRepository extends Repository<SubTipoCultivo>{

}