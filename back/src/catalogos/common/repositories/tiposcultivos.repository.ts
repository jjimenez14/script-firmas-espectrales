import { EntityRepository, Repository } from "typeorm";
import { TiposCultivos } from "../entities/tiposcultivos.entity";

@EntityRepository(TiposCultivos)
export class TiposCultivosRepository extends Repository<TiposCultivos>{

}