import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { EspecieEnfermedades } from "./especieenfermedades.entity";

@Entity()
export class SubtipoEnfermedad{

    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    nombre: string;

    @Column()
    activo: boolean;

    @OneToMany(()=> EspecieEnfermedades, EspEnf => EspEnf.subtipoenfermedad)
    especieEnf: EspecieEnfermedades

}