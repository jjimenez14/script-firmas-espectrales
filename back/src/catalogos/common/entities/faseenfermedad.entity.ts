import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class FaseEnfermerdad{

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    activo: boolean;

}