import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { SubtipoEnfermedad } from "./subtiposenfermedades.entity";

@Entity()
export class EspecieEnfermedades{

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    activo: boolean;

    @Column()
    avistamento: string;

    @ManyToOne(()=> SubtipoEnfermedad, subtipoEnf => subtipoEnf.especieEnf)
    subtipoenfermedad:SubtipoEnfermedad;


}