import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { SubTipoPlaga } from "./subtipoplaga.entity";

@Entity()
export class EspeciePlaga{

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    activo: boolean;


    @ManyToOne(()=> SubTipoPlaga, subTipoP => subTipoP.especiePlaga)
    subtipo: SubTipoPlaga
}