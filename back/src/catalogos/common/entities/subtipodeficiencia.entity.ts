import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class SubTipoDeficiencia {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    activo: boolean;

}