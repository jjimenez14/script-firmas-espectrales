import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";


@Entity()
export class FaseDeficiencia{

    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    nombre:string;

    @Column()
    activo: boolean;
}