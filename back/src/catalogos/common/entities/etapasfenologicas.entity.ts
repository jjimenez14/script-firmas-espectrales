import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class EtapasFenologicas {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    activo: boolean;

    @Column()
    cultivos: string;

}