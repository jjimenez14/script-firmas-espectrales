import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { SubTipoCultivo } from "./subtiposcultivos.entity";


@Entity()
export class TiposCultivos{

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    activo: boolean;

    @OneToMany(()=> SubTipoCultivo, subtipo => subtipo.tipoCultivo)
    tipos:SubTipoCultivo[]; 
}