import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";


@Entity()
export class DeterioroEficiencia{

    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    nombre:string;

    @Column()
    activo: boolean;

}