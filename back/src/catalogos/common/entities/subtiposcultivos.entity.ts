import { Column, Entity, ManyToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { TiposCultivos } from "./tiposcultivos.entity";


@Entity()
export class SubTipoCultivo {

     @PrimaryGeneratedColumn()
     id: number;

     @Column()
     nombre: string;

     @Column()
     activo: boolean;

     @ManyToOne(()=> TiposCultivos, tipos => tipos.tipos)
     tipoCultivo: TiposCultivos;

}