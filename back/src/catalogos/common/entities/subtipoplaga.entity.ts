import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { EspeciePlaga } from "./especieplaga.entity";

@Entity()
export class SubTipoPlaga{
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    activo: boolean;

    @OneToMany(()=> EspeciePlaga, EspPlaga => EspPlaga.subtipo)
    especiePlaga: EspeciePlaga;
}