import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class DeterioroEnfermedad{

    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    nombre:string;

    @Column()
    activo: boolean;

    @Column()
    avistamiento:string;
    

}