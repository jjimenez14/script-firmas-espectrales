import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class FasePlaga{

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    activo: boolean;

}