import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class DeterioroPlaga{

    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    nombre:string;

    @Column()
    activo: boolean;

    @Column()
    avistamiento:string;
}