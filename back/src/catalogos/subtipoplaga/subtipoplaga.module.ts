import { Module } from '@nestjs/common';
import { SubtipoplagaService } from './subtipoplaga.service';
import { SubtipoplagaController } from './subtipoplaga.controller';

@Module({
  providers: [SubtipoplagaService],
  controllers: [SubtipoplagaController]
})
export class SubtipoplagaModule {}
