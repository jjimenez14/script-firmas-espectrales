import { Module } from '@nestjs/common';
import { SubtiposcultivosService } from './subtiposcultivos.service';
import { SubtiposcultivosController } from './subtiposcultivos.controller';

@Module({
  providers: [SubtiposcultivosService],
  controllers: [SubtiposcultivosController]
})
export class SubtiposcultivosModule {}
