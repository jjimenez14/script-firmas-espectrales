import { Module } from '@nestjs/common';
import { FaseenfermedadplagaService } from './faseenfermedad.service';
import { FaseenfermedadController } from './faseenfermedad.controller';

@Module({
  providers: [FaseenfermedadplagaService],
  controllers: [FaseenfermedadController]
})
export class FaseenfermedadplagaModule {}
