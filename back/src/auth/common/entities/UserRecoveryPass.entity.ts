import { Column, CreateDateColumn, Entity, JoinColumn, ManyToMany, OneToOne, PrimaryGeneratedColumn,UpdateDateColumn} from 'typeorm';
import { Usuario } from './usuario.entity';

@Entity()
export class UserRecoveryPass{
    @PrimaryGeneratedColumn('increment')
    Id:number;
    
    @Column()
    TokenRecovery:string;

    @Column({default:true})
    Activo:boolean;
 
    @OneToOne(()=>Usuario)
    @JoinColumn({name:'id_usuario'})
    Usuario:Usuario;


    @UpdateDateColumn()
    Updated:Date;

    @CreateDateColumn()
    Created: Date;
}