import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn,UpdateDateColumn} from 'typeorm';
import { AliasContador } from './alias.entity';
import { Roles } from './roles.entity';
import { UserRecoveryPass } from './UserRecoveryPass.entity';


@Entity()
export class Usuario{
    @PrimaryGeneratedColumn('increment')
    Id:number; 

    @Column({length:100,unique:true})
    Email:string;

    @Column({length:100})
    Password:string;

    @Column({length:50})
    Nombre:string;

    @Column({length:50})
    Apaterno:string;
    
    @Column({length:50})
    Amaterno:string;

    @Column()
    Telefono:string; 
    
    @Column({default:true})
    Activo:boolean;

    @ManyToOne((type) =>Roles, rol =>rol.Usuario)
    @JoinColumn()
    Rol:Roles

    @OneToOne(()=>UserRecoveryPass,(userRecovery)=>userRecovery.Usuario)
    UserRecovery:UserRecoveryPass

    @OneToOne(()=>AliasContador,(alias)=>alias.Usuario)
    Alias:AliasContador


    @UpdateDateColumn()
    Updated:Date;

    @CreateDateColumn()
    Created: Date;
}