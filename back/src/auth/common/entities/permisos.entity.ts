import { Column, CreateDateColumn, Entity, ManyToMany, OneToOne, PrimaryGeneratedColumn,UpdateDateColumn} from 'typeorm';
import { Roles } from './roles.entity';

@Entity()
export class Permisos{
    @PrimaryGeneratedColumn('increment')
    Id:number;
    
    @Column({length:50})
    Nombre:string;

    @Column({default:true})
    Activo:boolean;

    @ManyToMany(()=>Roles,(tra)=>tra.Permisos)
    Permisos:Roles[]

    @UpdateDateColumn()
    Updated:Date;

    @CreateDateColumn()
    Created: Date;
}