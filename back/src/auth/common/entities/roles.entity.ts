import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Permisos } from './permisos.entity';
import { Usuario } from './usuario.entity';

@Entity()
export class Roles {
  @PrimaryGeneratedColumn('increment')
  Id: number;

  @Column({ length: 50 })
  Nombre: string;

  @Column({default:true})
  Activo: boolean;

  @ManyToMany(() => Permisos, (permis) => permis.Permisos)
  @JoinTable({
    name: 'roles_permisos',
    joinColumn: {
      name: 'roles_id',
    },
    inverseJoinColumn: { name: 'permisos_id' },
  })
  Permisos: Permisos[];

  //Relacion con los usuarios usuario
  @OneToMany( type=>Usuario, user => user.Rol)
  Usuario: Usuario[];

  @UpdateDateColumn()
  Updated: Date;

  @CreateDateColumn()
  Created: Date;
}
