import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn,UpdateDateColumn} from 'typeorm';
import { Usuario } from './usuario.entity';

@Entity()
export class AliasContador{
    @PrimaryGeneratedColumn('increment')
    Id:number;

    @Column({length:50})
    Alias:string;

    @Column()
    Contador:number;
    
    @Column({default:true})
    Activo:boolean;
    
    @OneToOne(()=>Usuario)
    @JoinColumn({name:'id_usuario'})
    Usuario:Usuario;

    @UpdateDateColumn()
    Updated:Date;

    @CreateDateColumn()
    Created: Date;
}