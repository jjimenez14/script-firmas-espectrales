import { IsEmail, IsNotEmpty, IsNumber, IsString} from "class-validator";

export class PermisosDto{
    @IsNotEmpty()
    @IsNumber()
    Id: number;

    @IsNotEmpty()
    @IsString()
    Nombre:string;
}