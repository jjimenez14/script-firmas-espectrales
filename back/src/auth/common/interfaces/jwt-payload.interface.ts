import { Roles } from "../entities/roles.entity";

export interface JwtPayLoad{
    Id:number;
    Email:string;
}