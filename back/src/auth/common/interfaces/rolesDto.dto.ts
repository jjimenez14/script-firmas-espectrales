import { IsEmail, IsNotEmpty, IsNumber, IsString} from "class-validator";
import { Permisos } from "../entities/permisos.entity";

export class RolesDto{
    @IsNotEmpty()
    @IsNumber()
    Id: number;

    @IsNotEmpty()
    @IsString()
    Nombre:string;

    @IsNotEmpty()
    Permisos:Permisos[]
}