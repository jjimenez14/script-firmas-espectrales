import { IsBoolean, IsDate, IsEmail, IsNotEmpty, IsNumber, IsOptional, IsString, Length } from 'class-validator';
import { Roles } from '../entities/roles.entity';

const minc=2; const maxc=50; 
const minpass=6;  const maxpass=20;
const tel=10


export class RegisterUsuarioDto {
  @IsNotEmpty()
  @IsEmail()
  @Length(minc, maxc)
  Email: string;

  @IsNotEmpty()
  @Length(minpass, maxpass)
  Password: string;

  @IsNotEmpty()
  @IsString()
  @Length(minc, maxc)
  Nombre: string;

  @IsNotEmpty()
  @Length(minc, maxc)
  Apaterno: string;

  @IsNotEmpty()
  @Length(minc, maxc)
  Amaterno: string;

  @IsNotEmpty()
  @IsString()
  @Length(tel,tel)
  Telefono:string 

  @IsNotEmpty()
  Rol:Roles
}

export class EditarUsuarioDto {
    @IsNotEmpty()
    @IsNumber()
    Id: number;

    @IsOptional()
    @IsNotEmpty()
    @IsEmail()
    @Length(minc, maxc)
    Email: string;
  
    @IsOptional()
    @IsNotEmpty()
    @Length(minpass, maxpass)
    Password: string;

    @IsOptional()
    @IsNotEmpty()
    @IsString()
    @Length(minc, maxc)
    Nombre: string;
  
    @IsOptional()
    @IsNotEmpty()
    @Length(minc, maxc)
    Apaterno: string;
  
    @IsOptional()
    @IsNotEmpty()
    @Length(minc, maxc)
    Amaterno: string;
  
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    @Length(tel,tel)
    Telefono:string 

    @IsOptional()
    @IsNotEmpty()
    Rol:Roles
    
  }
