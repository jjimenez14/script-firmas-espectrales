import { Injectable, UnauthorizedException } from '@nestjs/common';
import { RegisterUsuarioDto } from 'src/auth/common/interfaces';
import { PasswordService } from 'src/auth/security/password/password.service';
import { UsuarioRepository } from './usuario.repository';

@Injectable()
export class UsersService {

    constructor(
      ) {}

    async createUsuarioRoot(  
        clave:string,
        RegisterUsuarioDto: RegisterUsuarioDto
      ): Promise<boolean> {    
          
        if(clave!=='quieroserinnmortal'){
          throw new UnauthorizedException('la palabra clave no es correcta ');
        }
        return true
    
      }
}
