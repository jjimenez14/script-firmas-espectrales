import { EntityRepository, Repository } from 'typeorm';
import {
  ConflictException,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { NotFoundError } from 'rxjs';
import { Usuario } from 'src/auth/common/entities/usuario.entity';


@EntityRepository(Usuario)
export class UsuarioRepository extends Repository<Usuario> {
}


