import { Body, Controller, Param, Post } from '@nestjs/common';
import { RegisterUsuarioDto } from 'src/auth/common/interfaces';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
    constructor( private service:UsersService) {
    }

    
    @Post('/createRoot/:clave')
    createUsuarioRoot(@Param('clave') clave:string,@Body()RegisterUsuarioDto:RegisterUsuarioDto):Promise<boolean>{
        return this.service.createUsuarioRoot(clave,RegisterUsuarioDto)
    }


    

}
