import { EntityRepository, Repository } from 'typeorm';
import {
  ConflictException,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { NotFoundError } from 'rxjs';
import { Roles } from 'src/auth/common/entities/roles.entity';


@EntityRepository(Roles)
export class RolesRepository extends Repository<Roles> {

}
