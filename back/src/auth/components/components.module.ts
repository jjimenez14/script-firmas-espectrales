import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { SessionManagerModule } from './session-manager/session-manager.module';
import { RolesModule } from './roles/roles.module';
import { PermisosModule } from './permisos/permisos.module';

@Module({
  imports: [UsersModule, SessionManagerModule, RolesModule, PermisosModule],
  providers: []
})
export class ComponentsModule {}
 