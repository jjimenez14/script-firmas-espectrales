import { EntityRepository, Repository } from 'typeorm';
import {
  ConflictException,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { NotFoundError } from 'rxjs';
import { Permisos } from 'src/auth/common/entities/permisos.entity';


@EntityRepository(Permisos)
export class PermisosRepository extends Repository<Permisos> {

}
