import { Module } from '@nestjs/common';
import { SessionManagerService } from './session-manager.service';
import { SessionManagerController } from './session-manager.controller';

@Module({
  providers: [SessionManagerService],
  controllers: [SessionManagerController]
})
export class SessionManagerModule {}
