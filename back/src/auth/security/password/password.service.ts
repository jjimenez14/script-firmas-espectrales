import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

@Injectable()
export class PasswordService {
  async encodePassword(password: string): Promise<string> {
    const salt = await bcrypt.genSalt(); //Obteniendo el Salt
    return await bcrypt.hash(password, salt); //hash pasword
  }

  async checkPassword(
    password: string,
    userPassword: string,
  ): Promise<boolean> {
    return await bcrypt.compare(password, userPassword); //hash pasword
  }
}
