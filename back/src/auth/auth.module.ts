import { Module } from '@nestjs/common';
import { ComponentsModule } from './components/components.module';
import { SecurityService } from './security/security.service';
import { EmailsService } from './emails/emails.service';
import { EmailsController } from './emails/emails.controller';
import { PasswordService } from './security/password/password.service';

@Module({
  imports: [ComponentsModule],
  providers: [SecurityService, EmailsService, PasswordService],
  controllers: [EmailsController]
})
export class AuthModule {}
