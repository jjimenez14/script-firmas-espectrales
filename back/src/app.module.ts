import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { AvistamientosModule } from './avistamientos/avistamientos.module';
import { LaboratoriosModule } from './laboratorios/laboratorios.module';
import { CatalogosModule } from './catalogos/catalogos.module';
import { TypeOrmModule } from '@nestjs/typeorm';


@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: '',
    database: 'firmasDB',
    entities: [__dirname + '/**/*.entity{.ts,.js}'],
    synchronize: true,
  }),AuthModule, AvistamientosModule, LaboratoriosModule, CatalogosModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {} 
